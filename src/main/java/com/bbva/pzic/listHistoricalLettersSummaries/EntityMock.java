package com.bbva.pzic.listHistoricalLettersSummaries;

import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.util.mappers.ObjectMapperHelper;

/**
 * Created on 28/12/2020.
 *
 * @author Entelgy
 */
public class EntityMock {
    public static final String BUSINESS_ID = "00000655";

    private final ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();
    private static final EntityMock INSTANCE = new EntityMock();

    public static EntityMock getInstance() {
        return INSTANCE;
    }

    public DTOIntHistoricalLettersSummariesId getDTOIntHistoricalLettersSummariesId() {
        DTOIntHistoricalLettersSummariesId input = new DTOIntHistoricalLettersSummariesId();
        input.setBusinessId(BUSINESS_ID);
        return input;
    }

}
