package com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * Formato de datos <code>RIMRF41</code> de la transacci&oacute;n <code>RIF4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "RIMRF41")
@RooJavaBean
@RooSerializable
public class FormatoRIMRF41 {
	
	/**
	 * <p>Campo <code>ID</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "ID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
	private String id;
	
	/**
	 * <p>Campo <code>EFEDES</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "EFEDES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 32, longitudMaxima = 32)
	private String efedes;
	
}