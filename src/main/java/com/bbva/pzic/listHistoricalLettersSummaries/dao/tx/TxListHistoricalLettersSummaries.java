package com.bbva.pzic.listHistoricalLettersSummaries.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.*;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.tx.mapper.ITxListHistoricalLettersSummariesMapper;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/*
    Se utiliza los mapIn y mapOut del dao
 */
@Component
public class TxListHistoricalLettersSummaries extends DoubleOutputFormat<DTOIntHistoricalLettersSummariesId, FormatoRIMRF40, List<HistoricalLettersSummaries>, FormatoRIMRF41, FormatoRIMRF42> {
    @Resource(name = "txListHistoricalLettersSummariesMapper")
    private ITxListHistoricalLettersSummariesMapper mapper;

    @Autowired
    public TxListHistoricalLettersSummaries(@Qualifier("transaccionRif4") InvocadorTransaccion<PeticionTransaccionRif4, RespuestaTransaccionRif4> transaccion) {
        super(transaccion,PeticionTransaccionRif4::new, ArrayList::new,FormatoRIMRF41.class,FormatoRIMRF42.class);
    }

    @Override
    protected FormatoRIMRF40 mapInput(DTOIntHistoricalLettersSummariesId dtoIntHistoricalLettersSummariesId) {
        return mapper.mapIn(dtoIntHistoricalLettersSummariesId);
    }

    @Override
    protected List<HistoricalLettersSummaries> mapFirstOutputFormat(FormatoRIMRF41 formatoRIMRF41, DTOIntHistoricalLettersSummariesId dtoIntHistoricalLettersSummariesId, List<HistoricalLettersSummaries> historicalLettersSummaries) {
        return mapper.mapOut1(formatoRIMRF41, historicalLettersSummaries);
    }

    @Override
    protected List<HistoricalLettersSummaries> mapSecondOutputFormat(FormatoRIMRF42 formatoRIMRF42, DTOIntHistoricalLettersSummariesId dtoIntHistoricalLettersSummariesId, List<HistoricalLettersSummaries> historicalLettersSummaries) {
        return mapper.mapOut2(formatoRIMRF42, historicalLettersSummaries);
    }

}
