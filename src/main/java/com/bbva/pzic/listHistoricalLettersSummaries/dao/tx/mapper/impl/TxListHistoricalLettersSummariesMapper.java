package com.bbva.pzic.listHistoricalLettersSummaries.dao.tx.mapper.impl;

import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.tx.mapper.ITxListHistoricalLettersSummariesMapper;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalParameter;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.InformationPeriod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class TxListHistoricalLettersSummariesMapper implements ITxListHistoricalLettersSummariesMapper {

    private static final Log LOG = LogFactory.getLog(TxListHistoricalLettersSummariesMapper.class);

    @Override
    public FormatoRIMRF40 mapIn(DTOIntHistoricalLettersSummariesId dtoIn) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapIn ...");
        if(dtoIn == null)
            return null;
        FormatoRIMRF40 formatoRIMRF40 = new FormatoRIMRF40();
        formatoRIMRF40.setCodcent(dtoIn.getBusinessId());
        return formatoRIMRF40;
    }

    @Override
    public List<HistoricalLettersSummaries> mapOut1(FormatoRIMRF41 formatOutput, List<HistoricalLettersSummaries> historicalLettersSummaries) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapOut1 ...");
        if(historicalLettersSummaries == null)
           historicalLettersSummaries = new ArrayList<>();
        HistoricalLettersSummaries historicalLettersSummary = new HistoricalLettersSummaries();
        historicalLettersSummary.setId(formatOutput.getId());
        historicalLettersSummary.setName(formatOutput.getEfedes());
        historicalLettersSummaries.add(historicalLettersSummary);
        return historicalLettersSummaries;
    }

    @Override
    public List<HistoricalLettersSummaries> mapOut2(FormatoRIMRF42 formatOutput, List<HistoricalLettersSummaries> historicalLettersSummaries) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapOut2 ...");
        HistoricalLettersSummaries historicalLettersSummarie= new HistoricalLettersSummaries();
        if(!CollectionUtils.isEmpty(historicalLettersSummaries)){
            historicalLettersSummarie = historicalLettersSummaries.get(historicalLettersSummaries.size() - 1);
        }else{
            historicalLettersSummaries = new ArrayList<>();
        }
        if(CollectionUtils.isEmpty(historicalLettersSummarie.getHistoricalParameters())){
            historicalLettersSummarie.setHistoricalParameters(new ArrayList<>());
        }
        HistoricalParameter historicalParameters = new HistoricalParameter();
        historicalParameters.setPercentage(formatOutput.getPorefe());
        historicalParameters.setInformationPeriod(mapOutInformationPeriod(formatOutput.getPerefel(), formatOutput.getUniefel()));
        historicalLettersSummarie.getHistoricalParameters().add(historicalParameters);
        historicalLettersSummaries.add(historicalLettersSummarie);
        return historicalLettersSummaries;
    }

    private InformationPeriod mapOutInformationPeriod(final Integer perefel, final String uniefel) {
        if(perefel == null &&  uniefel == null){
            return null;
        }
        InformationPeriod informationPeriod = new InformationPeriod();
        informationPeriod.setNumber(perefel);
        informationPeriod.setUnit(uniefel);
        return informationPeriod;
    }
}
