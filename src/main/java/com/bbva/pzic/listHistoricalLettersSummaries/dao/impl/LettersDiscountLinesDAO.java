package com.bbva.pzic.listHistoricalLettersSummaries.dao.impl;

import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.ILettersDiscountLinesDAO;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.tx.TxListHistoricalLettersSummaries;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Component
public class LettersDiscountLinesDAO implements ILettersDiscountLinesDAO {
    private static final Log LOG = LogFactory.getLog(LettersDiscountLinesDAO.class);

    @Resource(name = "txListHistoricalLettersSummaries")
    private TxListHistoricalLettersSummaries txListHistoricalLettersSummaries;
    @Override
    public List<HistoricalLettersSummaries> listHistoricalLettersSummaries(DTOIntHistoricalLettersSummariesId input) {
        LOG.info("... Invoking method LettersDiscountLinesDAO.listHistoricalLettersSummaries ...");
        return txListHistoricalLettersSummaries.perform(input);
    }
}
