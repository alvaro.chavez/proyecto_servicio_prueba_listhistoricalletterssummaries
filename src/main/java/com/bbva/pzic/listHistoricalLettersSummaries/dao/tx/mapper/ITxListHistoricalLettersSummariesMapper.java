package com.bbva.pzic.listHistoricalLettersSummaries.dao.tx.mapper;

import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface ITxListHistoricalLettersSummariesMapper {
            // formato modelo
    public FormatoRIMRF40 mapIn(DTOIntHistoricalLettersSummariesId dtoIn);
    public List<HistoricalLettersSummaries> mapOut1(FormatoRIMRF41 format, List<HistoricalLettersSummaries> historicalLettersSummaries);
    public List<HistoricalLettersSummaries> mapOut2(FormatoRIMRF42 format, List<HistoricalLettersSummaries> historicalLettersSummaries);


}
