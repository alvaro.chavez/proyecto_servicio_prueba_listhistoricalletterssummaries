package com.bbva.pzic.listHistoricalLettersSummaries.dao;

import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface ILettersDiscountLinesDAO {
    List<HistoricalLettersSummaries> listHistoricalLettersSummaries (DTOIntHistoricalLettersSummariesId input);
}
