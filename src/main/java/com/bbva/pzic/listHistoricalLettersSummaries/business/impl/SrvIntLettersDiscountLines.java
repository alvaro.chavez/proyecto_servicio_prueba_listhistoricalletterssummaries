package com.bbva.pzic.listHistoricalLettersSummaries.business.impl;

import com.bbva.pzic.listHistoricalLettersSummaries.business.ISrvIntLettersDiscountLines;
import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.ValidationGroup;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.ILettersDiscountLinesDAO;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SrvIntLettersDiscountLines implements ISrvIntLettersDiscountLines {

    private static final Log LOG = LogFactory.getLog(SrvIntLettersDiscountLines.class);

    @Autowired
    private Validator validator;

    @Autowired
    private ILettersDiscountLinesDAO lettersDiscountLinesDAO;

    @Override
    public List<HistoricalLettersSummaries> listHistoricalLettersSummaries(DTOIntHistoricalLettersSummariesId input) {
        LOG.info("... Invoking method SrvIntLettersDiscountLines.listHistoricalLettersSummaries ...");
        LOG.info("... Validating listHistoricalLettersSumaries input parameter ...");
        validator.validate(input, ValidationGroup.HistoricalLettersSummariesId.class);
        return lettersDiscountLinesDAO.listHistoricalLettersSummaries(input);
    }
}
