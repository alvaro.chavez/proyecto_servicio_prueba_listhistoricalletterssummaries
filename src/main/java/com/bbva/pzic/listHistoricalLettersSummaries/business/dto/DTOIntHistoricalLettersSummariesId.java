package com.bbva.pzic.listHistoricalLettersSummaries.business.dto;

public class DTOIntHistoricalLettersSummariesId {

    private String businessId;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}