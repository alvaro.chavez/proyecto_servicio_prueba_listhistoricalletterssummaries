package com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "informationPeriod", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlType(name = "informationPeriod", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class InformationPeriod implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer number;
    private String unit;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
