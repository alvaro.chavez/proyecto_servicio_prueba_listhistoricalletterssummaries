package com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "historicalParameter", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlType(name = "historicalParameter", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class HistoricalParameter implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer percentage;
    private InformationPeriod informationPeriod;

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public InformationPeriod getInformationPeriod() {
        return informationPeriod;
    }

    public void setInformationPeriod(InformationPeriod informationPeriod) {
        this.informationPeriod = informationPeriod;
    }
}
