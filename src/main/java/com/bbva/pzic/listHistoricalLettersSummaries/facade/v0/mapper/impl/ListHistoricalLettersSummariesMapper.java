package com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.mapper.IListHistoricalLettersSummariesMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
public class ListHistoricalLettersSummariesMapper implements IListHistoricalLettersSummariesMapper {

    private static final Log LOG = LogFactory.getLog(ListHistoricalLettersSummariesMapper.class);

    @Override
    public DTOIntHistoricalLettersSummariesId mapIn(String businessId) {
    LOG.info("... called method ListHistoricalLettersSummariesMapper.mapIn ...");
        if(businessId == null)
            return null;
        DTOIntHistoricalLettersSummariesId dtoIn = new DTOIntHistoricalLettersSummariesId();
        dtoIn.setBusinessId(businessId);
        return dtoIn;
    }

    @Override
    public ServiceResponse<List<HistoricalLettersSummaries>> mapOut(List<HistoricalLettersSummaries> input) {
    LOG.info("... called method ListHistoricalLettersSummariesMapper.mapOut ...");
        if(CollectionUtils.isEmpty(input))
            return null;
        return ServiceResponse.data(input).build();
    }

}
