package com.bbva.pzic.listHistoricalLettersSummaries.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface ISrvLettersDiscountLinesV0 {
    ServiceResponse<List<HistoricalLettersSummaries>> listHistoricalLettersSummaries(String businessID);

}
