package com.bbva.pzic.listHistoricalLettersSummaries.facade.v0;

public class RegistryIds {
    public static final String SMC_REGISTRY_ID_OF_LIST_HISTORICAL_LETTERS_SUMMARIES = "SMGG20203815";

    private RegistryIds(){
    }
}
