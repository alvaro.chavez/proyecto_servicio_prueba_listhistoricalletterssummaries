package com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface IListHistoricalLettersSummariesMapper {
    DTOIntHistoricalLettersSummariesId mapIn(String businessId);
    ServiceResponse<List<HistoricalLettersSummaries>> mapOut (List<HistoricalLettersSummaries> input);

}
