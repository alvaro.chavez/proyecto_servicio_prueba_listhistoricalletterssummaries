package com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "historicalLettersSummaries", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlType(name = "historicalLettersSummaries", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class HistoricalLettersSummaries implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private List<HistoricalParameter> historicalParameters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HistoricalParameter> getHistoricalParameters() {
        return historicalParameters;
    }

    public void setHistoricalParameters(List<HistoricalParameter> historicalParameters) {
        this.historicalParameters = historicalParameters;
    }
}
