package com.bbva.pzic.listHistoricalLettersSummaries.dao.tx.mapper;

import com.bbva.pzic.listHistoricalLettersSummaries.EntityMock;
import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.listHistoricalLettersSummaries.dao.tx.mapper.impl.TxListHistoricalLettersSummariesMapper;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import com.bbva.pzic.listHistoricalLettersSummaries.FormatoRif4Stubs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ITxListHistoricalLettersSummariesMapperTest {

    private EntityMock entityMock = EntityMock.getInstance();

    @InjectMocks
    private TxListHistoricalLettersSummariesMapper mapper;

    private FormatoRif4Stubs formatoRif4Stubs = FormatoRif4Stubs.getInstance();

    @Test
    public void mapInFullTest() {
        DTOIntHistoricalLettersSummariesId input = entityMock.getDTOIntHistoricalLettersSummariesId();
        FormatoRIMRF40 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getCodcent());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoRIMRF40 result = mapper.mapIn(null);
        assertNull(result);
    }

    @Test
    public void mapOut1FullTest() throws IOException {
        FormatoRIMRF41 formatoRIMRF41 = formatoRif4Stubs.getFormatoRIMRF41();
        List<HistoricalLettersSummaries> historicalLettersSummariesList = new ArrayList<>();
        historicalLettersSummariesList=mapper.mapOut1(formatoRIMRF41,historicalLettersSummariesList);
        assertNotNull(historicalLettersSummariesList.get(0));
        assertNotNull(historicalLettersSummariesList.get(0).getName());
        assertNotNull(historicalLettersSummariesList.get(0).getId());
        assertEquals(formatoRIMRF41.getEfedes(),historicalLettersSummariesList.get(0).getName());
        assertEquals(formatoRIMRF41.getId(),historicalLettersSummariesList.get(0).getId());
    }

    @Test
    public void mapOut1EmptyTest() {
        List<HistoricalLettersSummaries> historicalLettersSummariesList = new ArrayList<>();
        historicalLettersSummariesList = mapper.mapOut1(new FormatoRIMRF41(),historicalLettersSummariesList);
        assertNotNull(historicalLettersSummariesList);
        assertNotNull(historicalLettersSummariesList.get(0));
        assertNull(historicalLettersSummariesList.get(0).getName());
        assertNull(historicalLettersSummariesList.get(0).getId());
        assertNull(historicalLettersSummariesList.get(0).getHistoricalParameters());
    }


    @Test
    public void mapOut2FullTest() throws IOException {
        FormatoRIMRF42 formatoRIMRF42 = formatoRif4Stubs.getFormatoRIMRF42();
        List<HistoricalLettersSummaries> historicalLettersSummaries = new ArrayList<>();
        historicalLettersSummaries = mapper.mapOut2(formatoRIMRF42, historicalLettersSummaries);
        HistoricalLettersSummaries result = historicalLettersSummaries.get(0);

        assertNotNull(historicalLettersSummaries);
        assertNotNull(result);
        assertNotNull(result.getHistoricalParameters());
        assertNotNull(result.getHistoricalParameters().get(0).getInformationPeriod());
        assertNotNull(result.getHistoricalParameters().get(0).getPercentage());
        assertNotNull(result.getHistoricalParameters().get(0).getInformationPeriod().getNumber());
        assertNotNull(result.getHistoricalParameters().get(0).getInformationPeriod().getUnit());

        assertEquals(formatoRIMRF42.getPerefel(),result.getHistoricalParameters().get(0).getInformationPeriod().getNumber());
        assertEquals(formatoRIMRF42.getUniefel(),result.getHistoricalParameters().get(0).getInformationPeriod().getUnit());
        assertEquals(formatoRIMRF42.getPorefe(),result.getHistoricalParameters().get(0).getPercentage());
    }
    @Test
    public void mapOut2EmptyTest() {
        List<HistoricalLettersSummaries> historicalLettersSummariesList = new ArrayList<>();
        historicalLettersSummariesList = mapper.mapOut2(new FormatoRIMRF42(),historicalLettersSummariesList);
        assertNotNull(historicalLettersSummariesList);
        assertNotNull(historicalLettersSummariesList.get(0));
        assertNull(historicalLettersSummariesList.get(0).getName());
        assertNull(historicalLettersSummariesList.get(0).getId());
        assertNotNull(historicalLettersSummariesList.get(0).getHistoricalParameters());
    }
}