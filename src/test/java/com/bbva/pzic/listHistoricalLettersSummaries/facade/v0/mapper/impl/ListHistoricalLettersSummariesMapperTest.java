package com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.listHistoricalLettersSummaries.EntityMock;
import com.bbva.pzic.listHistoricalLettersSummaries.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.listHistoricalLettersSummaries.facade.v0.dto.HistoricalLettersSummaries;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
public class ListHistoricalLettersSummariesMapperTest {

    private ListHistoricalLettersSummariesMapper mapper = new ListHistoricalLettersSummariesMapper();

    @Test
    public void mapInFullTest() {
        DTOIntHistoricalLettersSummariesId result = mapper.mapIn(EntityMock.BUSINESS_ID);
        assertNotNull(result);
        assertNotNull(result.getBusinessId());
        assertEquals(EntityMock.BUSINESS_ID,result.getBusinessId());
    }

    @Test
    public void mapInEmpyTest() {
        DTOIntHistoricalLettersSummariesId result=mapper.mapIn(null);
        assertNull(result);
     //   assertNull(result.getBusinessId());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<List<HistoricalLettersSummaries>> result = mapper.mapOut(Collections.emptyList());
        assertNull(result);
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<List<HistoricalLettersSummaries>> result = mapper.mapOut(Collections.singletonList(new HistoricalLettersSummaries()));
        assertNotNull(result);
        assertNotNull(result.getData());
    }
}